﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StudentPOC.Model;
using StudentPOC.ViewModel;

namespace StudentPOC.Views
{
    /// <summary>
    /// Interaction logic for DeleteStudent.xaml
    /// </summary>
    public partial class DeleteStudent : Page
    {
        public StudentInfo selectedStudent { get; set; }

        private List<StudentInfo> students { get; set; }

        public DeleteStudent()
        {
            InitializeComponent();
            var studentObject = new StudentViewModel();
            students = studentObject.Students;
            dgStudent.ItemsSource=students;
        }

        private void dgSudent_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedStudent = dgStudent.SelectedItem as StudentInfo;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            selectedStudent = dgStudent.SelectedItem as StudentInfo;
            if (selectedStudent == null)
            {
                MessageBox.Show("Cannot delete the blank Entry");
            }
            else
            {
                //students.Remove(selectedStudent);
                
                List<StudentInfo> studentInfos=dgStudent.ItemsSource as List<StudentInfo>;
                if (studentInfos.Count() == 2)
                {
                    var studentData = students.Where(x => x.Id != selectedStudent.Id).ToList();
                    dgStudent.ItemsSource = studentData;
                    MessageBox.Show("Record Deleted Successfully..");
                }
                if (studentInfos.Count() == 1)
                {
                    dgStudent.ItemsSource = new List<StudentInfo>();
                    MessageBox.Show("Record Deleted Successfully..");
                    MessageBox.Show("No Records Found..");
                }
            }
        }
    }
}
