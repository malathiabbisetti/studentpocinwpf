﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StudentPOC.Views
{
    /// <summary>
    /// Interaction logic for Registration.xaml
    /// </summary>
    public partial class Registration : Page
    {
        public Registration()
        {
            InitializeComponent();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            this.LablesClear();
            if (string.IsNullOrEmpty(txtRollNo.Text))
            {
               lblRollNoErrMsg.Content = "* Please Enter RollNo";
            }
            else if (string.IsNullOrEmpty(txtfirstName.Text))
            {
                lblFirstNameErrMsg.Content = "* Please Enter First Name";
            }
            else if (string.IsNullOrEmpty(txtLastName.Text))
            {
                lblLastNameErrMsg.Content = "* Please Enter Last Name";
            }
            else if (!string.IsNullOrEmpty(txtClass.Text) && (Convert.ToInt32(txtClass.Text) > 10 || Convert.ToInt32(txtClass.Text) == 0))
            {
                lblClassErrMsg.Content = "* Enter Valid Class(Less Than 11)";
            }
            else if (!string.IsNullOrEmpty(txtPercentage.Text) && (Convert.ToInt32(txtPercentage.Text) > 100 || Convert.ToInt32(txtClass.Text) < 0))
            {
                lblPercentageErrMsg.Content = "* Please Enter Valid Percentage";
            }
            else if (string.IsNullOrEmpty(txtGrade.Text))
            {
                lblGradeErrMsg.Content = "* Plaese Enter Grade";
            }
            else if (!string.IsNullOrEmpty(txtYOP.Text) && (Convert.ToInt32(txtYOP.Text.Length) < 0 || (Convert.ToInt32(txtYOP.Text.Length) < 4)))
            {
                lblYOPErrMsg.Content = "* Enter YOP like 2019";
            }
            else
            {
                MessageBox.Show("Data Submitted Successfully..");
                this.TextBoxClear();
                this.LablesClear();
            }
        }

        private void btnPerInfo_Click(object sender, RoutedEventArgs e)
        {
            this.LablesClear();

            if (string.IsNullOrEmpty(cmbStuRollNo.Text))
            {
                lblStuRollNoErrMsg.Content = "* Please Select Student Roll No";
            }
            else if (string.IsNullOrEmpty(txtFatherName.Text))
            {
                lblFNErrMsg.Content = "* Please Enter Father Name";
            }
            else if (string.IsNullOrEmpty(txtMotherName.Text))
            {
                lblMNErrMsg.Content = "* Please Enter Mother Name";
            }
            else if (rdFemale.IsChecked == false && rdMale.IsChecked == false)
            {
                lblGenderErrMsg.Content = "* Please Select Gender";
            }
            else if (string.IsNullOrEmpty(txtMobileNumber.Text))
            {
                lblMobNoErrMsg.Content = "* Please Enter Mobile Number";
            }
            else if (string.IsNullOrEmpty(txtHobbies.Text))
            {
                lblHobbiesErrMsg.Content = "* Please Enter Hobbies";
            }
            else if (string.IsNullOrEmpty(txtHouseNo.Text))
            {
                lblHouseNoErrMsg.Content = "* Please Enter House No";
            }
            else if (string.IsNullOrEmpty(txtStreetName.Text))
            {
                lblStreetNmErrMsg.Content = "* Please Enter Street Name";
            }
            else if (string.IsNullOrEmpty(cmbDistrict.Text))
            {
                lblDistrictErrMsg.Content = "* Please Select District";
            }
            else if (string.IsNullOrEmpty(cmbStates.Text))
            {
                lblStateErrMsg.Content = "* Please Select State";
            }
            else if (string.IsNullOrEmpty(txtPinCode.Text))
            {
                lblPINErrMsg.Content = "* Please Enter PIN CODE";
            }
            else
            {
                MessageBox.Show("Data Submitted Successfully...");
                this.TextBoxClear();
                this.LablesClear();
            }
        }

        private void btnMarksInfo_Click(object sender, RoutedEventArgs e)
        {
            this.LablesClear();
            if (string.IsNullOrEmpty(cmbStudentRollNo.Text))
            {
                lblErrorMessage.Content = "* Please Select Student Roll No";
            }
            else  if(string.IsNullOrEmpty(txtEnglishMarks.Text) || (!string.IsNullOrEmpty(txtEnglishMarks.Text) && (Convert.ToInt32(txtEnglishMarks.Text) < 0 || Convert.ToInt32(txtEnglishMarks.Text) > 100)))
            {
                lblErrorMessage.Content = "* Enter English Marks between 0,100";
            }
            else if (string.IsNullOrEmpty(cmbEnglishGrade.Text))
            {
                lblErrorMessage.Content = "* Please Select English Grade";
            }
            else if(string.IsNullOrEmpty(txtTeluguMarks.Text) || (!string.IsNullOrEmpty(txtTeluguMarks.Text) && (Convert.ToInt32(txtTeluguMarks.Text)<0 || Convert.ToInt32(txtTeluguMarks.Text) > 100)))
            {
                lblErrorMessage.Content = "* Enter Telugu Marks between 0,100";
            }
            else if (string.IsNullOrEmpty(cmbTeluguGrade.Text))
            {
                lblErrorMessage.Content = "* Please Select Telugu Grade";
            }
            else if (string.IsNullOrEmpty(txtHindiMarks.Text) || (!string.IsNullOrEmpty(txtHindiMarks.Text) && (Convert.ToInt32(txtHindiMarks.Text) < 0 || Convert.ToInt32(txtHindiMarks.Text) > 100)))
            {
                lblErrorMessage.Content = "* Enter Hindi Marks between 0,100";
            }
            else if (string.IsNullOrEmpty(cmbHindiGrade.Text))
            {
                lblErrorMessage.Content = "* Please Select Hindi Grade";
            }
            else if (string.IsNullOrEmpty(txtMathsMarks.Text) || (!string.IsNullOrEmpty(txtMathsMarks.Text) && (Convert.ToInt32(txtMathsMarks.Text) < 0 || Convert.ToInt32(txtMathsMarks.Text) > 100)))
            {
                lblErrorMessage.Content = "* Enter Maths Marks between 0,100";
            }
            else if (string.IsNullOrEmpty(cmbMathsGrade.Text))
            {
                lblErrorMessage.Content = "* Please Select Maths Grade";
            }
            else if (string.IsNullOrEmpty(txtScienceMarks.Text) || (!string.IsNullOrEmpty(txtScienceMarks.Text) && (Convert.ToInt32(txtScienceMarks.Text) < 0 || Convert.ToInt32(txtScienceMarks.Text) > 100)))
            {
                lblErrorMessage.Content = "* Enter Science Marks between 0,100";
            }
            else if (string.IsNullOrEmpty(cmbScienceGrade.Text))
            {
                lblErrorMessage.Content = "* Please Select Science Grade";
            }
            else if (string.IsNullOrEmpty(txtSocialMarks.Text) || (!string.IsNullOrEmpty(txtSocialMarks.Text) && (Convert.ToInt32(txtSocialMarks.Text) < 0 || Convert.ToInt32(txtSocialMarks.Text) > 100)))
            {
                lblErrorMessage.Content = "* Enter Social Marks between 0,100";
            }
            else if (string.IsNullOrEmpty(cmbSocialGrade.Text))
            {
                lblErrorMessage.Content = "* Please Select Social Grade";
            }
            else
            {
                MessageBox.Show("Data Submitted Successfully..");
                this.TextBoxClear();
                this.LablesClear();
            }
        }

        private void TextBoxClear()
        {
            txtRollNo.Clear();
            txtfirstName.Clear();
            txtLastName.Clear();
            txtGrade.Clear();
            txtClass.Text = "0";
            txtPercentage.Text = "0";
            txtYOP.Text = "0";
            txtFatherName.Clear();
            txtMotherName.Clear();
            rdFemale.IsChecked = false;
            rdMale.IsChecked = false;
            txtMobileNumber.Clear();
            txtHobbies.Clear();
            txtHouseNo.Clear();
            txtStreetName.Clear();
            txtPinCode.Clear();
            cmbDistrict.Text = string.Empty;
            cmbStates.Text = string.Empty;
            cmbStuRollNo.Text = string.Empty;
            cmbStudentRollNo.Text = string.Empty;
            txtEnglishMarks.Clear();
            txtTeluguMarks.Clear();
            txtHindiMarks.Clear();
            txtMathsMarks.Clear();
            txtScienceMarks.Clear();
            txtSocialMarks.Clear();
            cmbEnglishGrade.Text = string.Empty;
            cmbTeluguGrade.Text = string.Empty;
            cmbHindiGrade.Text = string.Empty;
            cmbMathsGrade.Text = string.Empty;
            cmbScienceGrade.Text = string.Empty;
            cmbSocialGrade.Text = string.Empty;
        }

        private void LablesClear()
        {
            lblRollNoErrMsg.Content = string.Empty;
            lblFirstNameErrMsg.Content = string.Empty;
            lblLastNameErrMsg.Content = string.Empty;
            lblPercentageErrMsg.Content = string.Empty;
            lblGradeErrMsg.Content = string.Empty;
            lblClassErrMsg.Content = string.Empty;
            lblPercentageErrMsg.Content = string.Empty;
            lblYOPErrMsg.Content = string.Empty;
            lblFNErrMsg.Content = string.Empty;
            lblMNErrMsg.Content = string.Empty;
            lblMobNoErrMsg.Content = string.Empty;
            lblHobbiesErrMsg.Content = string.Empty;
            lblHouseNoErrMsg.Content = string.Empty;
            lblDistrictErrMsg.Content = string.Empty;
            lblStateErrMsg.Content = string.Empty;
            lblStreetNmErrMsg.Content = string.Empty;
            lblPINErrMsg.Content = string.Empty;
            lblGenderErrMsg.Content = string.Empty;
            lblStuRollNoErrMsg.Content = string.Empty;
            lblErrorMessage.Content = string.Empty;

        }
    }
}
