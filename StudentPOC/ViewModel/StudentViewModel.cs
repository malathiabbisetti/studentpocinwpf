﻿using GalaSoft.MvvmLight;
using StudentPOC.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentPOC.ViewModel
{
    public class StudentViewModel : ViewModelBase
    {
        public List<StudentInfo> Students { get; set; }

        public StudentViewModel()
        {
            Students = new List<StudentInfo>();

            StudentInfo student = new StudentInfo() 
            {
                Id = 1, RollNo = "STU001", FirstName = "Malathi", LastName = "Abbisetti", Class = 10, Percentage = 86, Grade = "A", YearOfPass = 2010,
                studentPersonalInfos = new List<StudentPersonalInfo>()
                {
                     new StudentPersonalInfo()
                     {
                         Id = 1,
                         StudentId = 1,
                         FatherName = "Srinivasa Rao",
                         MotherName = "Sridevi",
                         MobileNumber = "9505349931",
                         Gender = "Female",
                         Address = "RamannaPalem, Mogaltur Mandal, W.G.Dt, Andhra Pradesh, 534280",
                         Hobbies = "Spending Time with kids, Listening Music"
                     }
                },
                StudentMarksInfos = new List<StudentMarksInfo>()
                {
                    new StudentMarksInfo(){Id = 1, StudentId = 1, Subject = "English", Marks = 95, SubjectGrade = "A"},
                    new StudentMarksInfo(){Id = 2, StudentId = 1, Subject = "Telugu", Marks = 90, SubjectGrade = "A"},
                    new StudentMarksInfo(){Id = 3, StudentId = 1, Subject = "Hindi", Marks=85, SubjectGrade="B"}
                }
            };
            Students.Add(student);
            student = new StudentInfo()
            {
                Id = 2,
                RollNo = "STU002",
                FirstName = "Shanmitha",
                LastName = "Grandhi",
                Class = 5,
                Percentage = 85,
                Grade = "A",
                YearOfPass = 2020,
                studentPersonalInfos = new List<StudentPersonalInfo>()
                {
                    new StudentPersonalInfo(){Id = 2, StudentId = 2, FatherName = "Srinivas", MotherName = "Bharathi", MobileNumber = "9989007369", Gender = "Female", Hobbies = "Watching TV", Address = "Palakol, W.G.Dt, Andhra Pradesh, 534275"}
                },
                StudentMarksInfos = new List<StudentMarksInfo>()
                {
                    new StudentMarksInfo(){Id = 4, StudentId = 2, Subject = "English", Marks = 95, SubjectGrade = "A"},
                    new StudentMarksInfo(){Id = 5, StudentId = 2, Subject = "Telugu", Marks = 90, SubjectGrade = "A"},
                    new StudentMarksInfo(){Id = 6, StudentId = 2, Subject = "Hindi", Marks=85, SubjectGrade="B"},
                    new StudentMarksInfo(){Id = 7, StudentId = 2, Subject = "Maths", Marks = 98.5, SubjectGrade = "A"}
                }
            };
            Students.Add(student);
        }
    }
}
