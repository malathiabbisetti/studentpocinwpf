﻿using GalaSoft.MvvmLight;
using StudentPOC.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentPOC.ViewModel
{ 
    public class AddressViewModel : ViewModelBase
    {
        public List<Address> Addresses { get; set; }

        public List<Address> Districts { get; set; } 

        public List<Address> States { get; set; }

        public AddressViewModel()
        {
            Addresses = ListOfAddress();
            Districts = ListOfAddress().GroupBy(x => x.District).Select(y=>y.First()).ToList();
            States = ListOfAddress().GroupBy(x => x.State).Select(y => y.First()).ToList();
        }

        private List<Address> ListOfAddress()
        {
            List<Address> addresses= new List<Address>()
            {
                new Address(){Id = 1, District = "W.G.Dt", State = "Andhra Pradesh"},
                new Address(){Id = 2, District = "E.G.Dt", State = "Andhra Pradesh"},
                new Address(){Id = 3, District = "Eluru", State = "Andhra Pradesh"},
                new Address(){Id = 4, District = "Vizag", State="Andhra Pradesh"},
                new Address(){Id = 5, District = "Kammam", State="Telangana"},
                new Address(){Id = 6, District = "Karim Nagar", State="Telangana"}
            };

            return addresses;
        }
    }
}
