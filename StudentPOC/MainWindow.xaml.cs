﻿using StudentPOC.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StudentPOC
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddressMenu_Click(object sender,RoutedEventArgs e)
        {
            MainFrame.Content = new Views.Address();
        }

        private void StudentMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new Views.Student();
        }

        private void RegistrationMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new Views.Registration();
        }

        private void DeleteStudentMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new Views.DeleteStudent();
        }
    }
}
