﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentPOC.Model
{
    public class StudentPersonalInfo : INotifyPropertyChanged
    {
        private int id { get; set; }

        private int studentId { get; set; }

        private string fatherName { get; set; }

        private string motherName { get; set; }

        private string gender { get; set; }

        private string mobileNumber { get; set; }

        private string hobbies { get; set; }

        private string address { get; set; }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }

        public int StudentId
        {
            get
            {
                return studentId;
            }
            set
            {
                studentId = value;
                OnPropertyChanged("StudentId");
            }
        }

        public string FatherName 
        {
            get
            {
                return fatherName;
            }
            set
            {
                fatherName = value;
                OnPropertyChanged("FatherName");
            }
        }

        public string MotherName 
        {
            get
            {
                return motherName;
            }
            set
            {
                motherName = value;
                OnPropertyChanged("MotherName");
            }
        }

        public string Gender 
        {
            get
            {
                return gender;
            }
            set
            {
                gender = value;
                OnPropertyChanged("Gender");
            }
        }

        public string Address 
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
                OnPropertyChanged("Address");
            }
        }

        public string Hobbies 
        {
            get
            {
                return hobbies;
            }
            set
            {
                hobbies = value;
                OnPropertyChanged("Hobbies");
            }
        }

        public string MobileNumber
        {
            get
            {
                return mobileNumber;
            }
            set
            {
                mobileNumber = value;
                OnPropertyChanged("MobileNumber");
            }
        }


        #region INotifyPropertyChangedImplementation

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
