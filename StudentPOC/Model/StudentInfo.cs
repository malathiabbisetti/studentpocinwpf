﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StudentPOC.Model
{
    public class StudentInfo : INotifyPropertyChanged
    {
        private int id { get; set; }

        private string rollNo { get; set; }

        private string firstName { get; set; }

        private string lastName { get; set; }

        private int studentClass { get; set; }

        private int yearOfPass { get; set; }

        private int percentage { get; set; }

        private string grade { get; set; }

        public  int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }

        
        public string RollNo 
        {
            get { return rollNo; }
            set
            {
                rollNo = value;
                OnPropertyChanged("RollNo");
            }
        }

        public string FirstName 
        {
            get { return firstName; }
            set
            {
                firstName = value;
                OnPropertyChanged("FirstName");
            }
        }

        public string LastName 
        {
            get { return lastName; }
            set
            {
                lastName = value;
                OnPropertyChanged("LastName");
            }
        }

        public int Class 
        {
            get { return studentClass; }
            set
            {
                studentClass = value;
                OnPropertyChanged("Class");
            }
        }

        public int YearOfPass 
        {
            get { return yearOfPass; }
            set
            {
                yearOfPass = value;
                OnPropertyChanged("YearOfPass");
            }
        }

        public int Percentage 
        {
            get { return percentage; }
            set
            {
                percentage = value;
                OnPropertyChanged("Percentage");
            }
        }

        public string Grade
        {
            get { return grade; }
            set
            {
                grade = value;
                OnPropertyChanged("Grade");
            }
        }

        public List<StudentPersonalInfo> studentPersonalInfos { get; set; }

        public List<StudentMarksInfo> StudentMarksInfos { get; set; }

       

        #region INotifyPropertyChangedImplementation

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
