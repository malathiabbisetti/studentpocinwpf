﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentPOC.Model
{
    public class Address
    {
        public int Id { get; set; }

        public string District { get; set; }

        public string State { get; set; }
    }
}
