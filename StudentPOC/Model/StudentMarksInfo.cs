﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentPOC.Model
{
    public class StudentMarksInfo : INotifyPropertyChanged
    {
        private int id { get; set; }

        private int studentId { get; set; }

        private string subject { get; set; }

        private double marks { get; set; }

        private string subjectGrade { get; set; }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("Id");
            }
        }

        public int StudentId 
        {
            get
            {
                return studentId;
            }
            set
            {
                studentId = value;
                OnPropertyChanged("StudentId");
            }
        }

        public string Subject 
        {
            get
            {
                return subject;
            }
            set
            {
                subject = value;
                OnPropertyChanged("Subject");
            }
        }

        public double Marks 
        {
            get
            {
                return marks;
            }
            set
            {
                marks = value;
                OnPropertyChanged("Marks");
            }
        }

        public string SubjectGrade 
        {
            get
            {
                return subjectGrade;
            }
            set
            {
                subjectGrade = value;
                OnPropertyChanged("SubjectGrade");
            }
        }

        #region INotifyPropertyChangedImplementation

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when [property changed].
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null) PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
